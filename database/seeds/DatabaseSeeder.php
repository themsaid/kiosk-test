<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 10)->create();

        $products = factory(\App\Product::class, 10)->create();

        factory(\App\Tag::class, 20)->create()->each(function ($tag) use ($products) {
            $tag->products()->attach(
                $products->random(3)->pluck('id')->toArray()
            );
        });

        factory(\App\Order::class, 20)->create()->each(function ($tag) use ($products) {
            $tag->products()->attach($products->random(), ['quantity' => 1, 'price' => 30]);
            $tag->products()->attach($products->random(), ['quantity' => 1, 'price' => 30]);
        });
    }
}
