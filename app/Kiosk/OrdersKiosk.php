<?php

namespace App\Kiosk;

use App\Order;
use App\Product;
use Illuminate\Foundation\Auth\User;
use Laravel\Kiosk\Kiosk;
use Laravel\Kiosk\AbstractKiosk;

class OrdersKiosk extends AbstractKiosk
{
    /**
     * @inheritDoc
     */
    public $model = Order::class;

    /**
     * @inheritDoc
     */
    protected function browsingColumns()
    {
        return [
            Kiosk::column('#')->displays('id'),
            Kiosk::column('User')->displays('user.name'),
            Kiosk::column('Amount')->displays('price'),
            Kiosk::column('Submitted')->displays('created_at'),
            Kiosk::column('Products')->displays(function ($record) {
                return $record->products()->pluck('name');
            })->asLabel(),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function browsingFilters()
    {
        return [
            Kiosk::filter('User', 'user_id')
                ->asSelectBox(function () {
                    return ['' => 'All'] + User::all()->pluck('name', 'id')->toArray();
                }),

            Kiosk::filter('Product', 'product_id')
                ->asSelectBox(function () {
                    return ['' => 'All'] + Product::all()->pluck('name', 'id')->toArray();
                })->findsWith(function ($builder, $value) {
                    $builder->whereHas('products', function ($query) use ($value) {
                        $query->where('id', $value);
                    });
                })
        ];
    }

    /**
     * @inheritDoc
     */
    public function editorInputs()
    {
        return [
            Kiosk::input('User', 'user_id')->asSelectBox(function () {
                return User::all()->pluck('name', 'id');
            }),

            Kiosk::input('Products', 'products')->asTableOptions(function () {
                return [
                    Kiosk::tableOptionsInput('Product', 'product_id')->asSelectBox(function(){
                        return Product::all()->pluck('name', 'id')->toArray();
                    }),
                    Kiosk::tableOptionsInput('Quantity', 'quantity')->asTextInput(),
                    Kiosk::tableOptionsInput('Price', 'price')->asTextInput()
                ];
            }),
        ];
    }

    /**
     * @inheritDoc
     */
    public function store($model)
    {
        validator($this->request->all(), [
            'name' => 'required',
            'description' => 'required',
            'stock' => 'integer|min:0',
            'image' => 'image',
        ])->validate();


        $data = $this->request->only([
            'name', 'description', 'stock', 'is_available'
        ]);

        $model->fill($data);

        $model->save();

        $model->tags()->sync($this->request->tags);
    }

    /**
     * @inheritDoc
     */
    public function loadModelForJson($id)
    {
        $model = parent::loadModelForJson($id);

        $model->load('products:id');

        $results = $model->toArray();

        $results['products'] = $model->products->map(function($product){
            return [
                'product_id' => $product->id,
                'price' => $product->pivot->price,
                'quantity' => $product->pivot->quantity,
            ];
        });

        return $results;
    }
}