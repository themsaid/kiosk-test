<?php

namespace App\Kiosk;

use App\User;
use Illuminate\Validation\Rule;
use Laravel\Kiosk\Kiosk;
use Laravel\Kiosk\AbstractKiosk;

class UsersKiosk extends AbstractKiosk
{
    /**
     * @inheritDoc
     */
    public $model = User::class;

    /**
     * @inheritDoc
     */
    protected function browsingColumns()
    {
        return [
            Kiosk::column('#')->displays('id'),
            Kiosk::column('Name')->displays('name'),
            Kiosk::column('Email')->displays('email'),
            Kiosk::column('Type')->displays(function ($record) {
                return $record->is_admin ? 'Admin' : 'Customer';
            })->asLabel(function ($record, $value) {
                return $record->is_admin ? 'warning' : 'success';
            }),
            Kiosk::column('Orders')->displays(function ($record) {
                return $record->orders()->count().' Orders(s)';
            })->asLink(function ($row) {
                return url('kiosk/orders?filters[user_id]='.$row->id);
            })
        ];
    }

    /**
     * @inheritDoc
     */
    protected function browsingFilters()
    {
        return [
            Kiosk::filter('Name', 'name')
                ->asTextInput()
                ->findsRecordsLike(),

            Kiosk::filter('Email', 'email')
                ->asTextInput()
                ->findsRecordsLike(),

            Kiosk::filter('Customer', 'is_admin')
                ->asRadioInput(function () {
                    return [
                        '' => 'All',
                        0 => 'Customer',
                        1 => 'Admin',
                    ];
                })
        ];
    }

    /**
     * @inheritDoc
     */
    public function editorInputs()
    {
        return [
            Kiosk::input('Name', 'name')->asTextInput(),
            Kiosk::input('Email', 'email')->asTextInput(),
            Kiosk::input('Password', 'password')->asPasswordInput(),
            Kiosk::input('Type', 'is_admin')->asRadioInput(function () {
                return [
                    0 => 'Customer',
                    1 => 'Admin',
                ];
            }),
        ];
    }

    /**
     * @inheritDoc
     */
    public function store($model)
    {
        validator($this->request->all(), [
            'name' => 'required',
            'email' => 'required|email|'.Rule::unique('users')->ignore($model->id),
            'password' => ! $model->exists ? 'required' : '',
        ])->validate();

        parent::store($model);
    }
}