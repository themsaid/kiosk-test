<?php

namespace App\Kiosk;

use App\Product;
use App\Tag;
use Illuminate\Validation\Rule;
use Laravel\Kiosk\Kiosk;
use Laravel\Kiosk\AbstractKiosk;

class ProductsKiosk extends AbstractKiosk
{
    /**
     * @inheritDoc
     */
    public $model = Product::class;

    /**
     * @inheritDoc
     */
    protected function browsingColumns()
    {
        return [
            Kiosk::column('#')->displays('id'),
            Kiosk::column('')->displays('image')->asImage(),
            Kiosk::column('Name')->displays('name'),
            Kiosk::column('Stock')->displays('stock'),
            Kiosk::column('')->displays('is_available')->asOnOff(),
            Kiosk::column('Added')->displays(function($record){
                return $record->created_at->toDateString();
            }),
            Kiosk::column('Tags')->displays(function ($record) {
                return $record->tags()->select('name')->get()->pluck('name');
            })->asLabel(),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function browsingFilters()
    {
        return [
            Kiosk::filter('Name', 'name')
                ->asTextInput()
                ->findsRecordsLike(),

            Kiosk::filter('Stock', 'stock')
                ->asRadioInput(function () {
                    return ['' => 'All', 0 => 'Out of stock', 1 => 'In Stock',];
                })->findsWith(function ($builder, $value) {
                    $builder->where('stock', $value ? '>' : '<=', 0);
                }),

            Kiosk::filter('Availability', 'is_available')
                ->asRadioInput(function () {
                    return ['' => 'All', 0 => 'No', 1 => 'yes',];
                }),

            Kiosk::filter('Tag', 'tag_id')
                ->asSelectBox(function () {
                    return ['' => 'All'] + Tag::all()->pluck('name', 'id')->toArray();
                })->findsWith(function ($builder, $value) {
                    $builder->whereHas('tags', function ($query) use ($value) {
                        $query->where('id', $value);
                    });
                })
        ];
    }

    /**
     * @inheritDoc
     */
    public function editorInputs()
    {
        return [
            Kiosk::input('Name', 'name')->asTextInput(),

            Kiosk::input('Description', 'description')->asTextArea(),

            Kiosk::input('Stock', 'stock'),

            Kiosk::input('Image', 'image')->asImageUpload(true),

            Kiosk::input('Tags', 'tags')->asSelectBox(function () {
                return Tag::all()->pluck('name', 'id');
            }, true),

            Kiosk::input('Availability', 'is_available')->asCheckboxInput(function () {
                return [1 => 'Product is available'];
            }),
        ];
    }

    /**
     * @inheritDoc
     */
    public function store($model)
    {
        validator($this->request->all(), [
            'name' => 'required',
            'description' => 'required',
            'stock' => 'integer|min:0',
            'image' => 'image',
        ])->validate();


        $data = $this->request->only([
            'name', 'description', 'stock', 'is_available'
        ]);

        if ($this->request->hasFile('image')) {
            $data['image'] = $this->request->image->store('images');
        }

        $model->fill($data);

        $model->save();

        $model->tags()->sync($this->request->tags);
    }

    /**
     * @inheritDoc
     */
    public function loadModelForJson($id)
    {
        $model = parent::loadModelForJson($id);

        $model->load('tags:id');

        $results = $model->toArray();

        $results['tags'] = $model->tags->pluck('id');

        return $results;
    }
}